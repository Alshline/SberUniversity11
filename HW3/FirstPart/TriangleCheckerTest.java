
package HW3.FirstPart;

//Я так понял, что суть тестирования - получить результат false, когда в действительност он должен быть true и наоборот.
public class TriangleCheckerTest {
    public static void main(String[] args) {
        double a = 0;
        for (int i = 0; i < 20; i++) {
            a += 0.1;
        }
        System.out.println(TriangleChecker.checkTriangle(1, a, 3));
        System.out.println(TriangleChecker.checkTriangle(a, 2.5, 4.5));
    }
}
