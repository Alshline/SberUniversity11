package ProfileHW1;

import java.io.IOException;

public class MyCheckedException extends Exception {
    public MyCheckedException(String message) {
        super(message);
    }
}
