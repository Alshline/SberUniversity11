package ProfileHW6;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements DaoInterface<User> {

    private EntityManager entityManager =
            Persistence.createEntityManagerFactory("SberUniversity")
                    .createEntityManager();
    private List<User> users = new ArrayList<>();

    @Override
    public User add(User user) {
        entityManager.getTransaction().begin();
        User userFromDB = entityManager.merge(user);
        entityManager.getTransaction().commit();
        return userFromDB;
    }

    @Override
    public void delete(long id) {
        entityManager.getTransaction().begin();
        entityManager.remove(get(id));
        entityManager.getTransaction().commit();
    }

    @Override
    public User get(long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public void update(User user) {
        entityManager.getTransaction().begin();
        entityManager.merge(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<User> getAll() {
        TypedQuery<User> userTypedQuery = entityManager.createNamedQuery("User.getAll", User.class);
        return userTypedQuery.getResultList();
    }

    public User getUserByEmail(String email) {
        User user = null;
        try {
            TypedQuery<User> userTypedQuery = entityManager.createQuery("select c from User c where email = ?1", User.class);
            user = userTypedQuery.setParameter(1, email).getSingleResult();
        } catch (Exception exception){
            exception.printStackTrace();
        }
        return user;
    }
}
