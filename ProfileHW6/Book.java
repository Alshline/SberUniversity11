package ProfileHW6;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Books")
@NamedQuery(name = "Book.getAll", query = "SELECT c from Book c")
public class Book {

    @Id
    @Getter
    @GeneratedValue (strategy = GenerationType.AUTO)
    private long Id;

    @Getter
    @Setter
    @Column(name = "bookName")
    private String bookName;

    @Override
    public String toString() {
        return "Book{" +
                "Id=" + Id +
                ", bookName='" + bookName + '\'' +
                '}';
    }
}
