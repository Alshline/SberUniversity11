package ProfileHW1;

public class MyEvenNumber {
    private int number;

    public MyEvenNumber(int number) throws OddException {
        isOdd(number);
        this.number = number;
        printNumber();
    }

    public static void main(String[] args) {
        MyEvenNumber myEvenNumber = new MyEvenNumber(4);
    }

    void isOdd(int number) throws OddException {
        if ((number % 2 != 0) && (number > 0)) throw new OddException("Value is odd");
    }

    void printNumber() {
        System.out.println(number);
    }

    private class OddException extends RuntimeException {
        public OddException(String message) {
            super(message);
        }
    }
}
