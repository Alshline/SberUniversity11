package ProfileHW1;

/**
 * Проверка созданных проверяемого и непроверояемого исключений (MyCheckedException/MyUncheckedException в отделных классах)
 * Первое исключение на выбор индекса за границой встроенного массива, второе на наличие чисел в строке ввода.
 */

import java.util.Scanner;

public class CheckExceptionsClass {
    int[] someArray = new int[10];
    Scanner scanner;

    public CheckExceptionsClass() {
        scanner = new Scanner(System.in);
    }

    boolean hasNext() throws MyCheckedException {
        if (!scanner.hasNextInt()) {
            throw new MyCheckedException("String without numbers");
        } else {
            return true;
        }
    }

    boolean hasIndex(int index) throws MyUncheckedException {
        if (index > someArray.length) {
            throw new MyUncheckedException("Some index out of bounds");
        } else return true;
    }

    public static void main(String[] args) {
        CheckExceptionsClass checkExceptionsClass = new CheckExceptionsClass();
        //checkExceptionsClass.hasIndex(12);
        try {
            boolean hasNext = checkExceptionsClass.hasNext();
        } catch (MyCheckedException e) {
            e.printStackTrace();
        }

    }
}
