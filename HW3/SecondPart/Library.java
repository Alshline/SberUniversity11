package HW3.SecondPart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Library {
    private List<Book> booksList;
    private Map<Book, Visitor> visitorsMap;
    Estimation estimationPart;
    private Integer startId = 1;

    public Library() {
        System.out.println("Library added.");
        booksList = new ArrayList<>();
        visitorsMap = new HashMap<>();
        estimationPart = new Estimation();
    }

    public void addBook(Book book) {
        if (booksList.contains(book)) {
            System.out.println("This book " + book + " already here!");
            return;
        }
        System.out.println("Book " + book + " added.");
        booksList.add(book);
    }

    public void removeBook(Book book) {
        if (booksList.contains(book)) {
            if (visitorsMap.containsKey(book)) {
                System.out.println("Cant delete this book, coz its removed by Visitor.");
            } else {
                System.out.println("Book " + book + " removed.");
                booksList.remove(book);
            }
        } else {
            System.out.println("We cant delete, coz we dont have that book.");
        }
    }

    public Book getBookFromLibrary(Book book) {
        if (visitorsMap.containsKey(book)) {
            System.out.println("This book already taken!");
            return null;
        } else if (booksList.contains(book)) {
            Book foundedBook = booksList.get(booksList.indexOf(book));
            System.out.println("Gotted book " + foundedBook);
            return foundedBook;
        } else {
            System.out.println("We cant find this book.");
            return null;
        }
    }

    public List<Book> getListByAuthor(String author) {
        if (!checkAuthor(author)) {
            System.out.println("Wrong author");
            return null;
        }
        List<Book> newList = new ArrayList<>();
        for (int i = 0; i < booksList.size(); i++) {
            if (booksList.get(i).getAuthorName().equals(author)) {
                Book foundedBook = booksList.get(i);
                newList.add(foundedBook);
                System.out.println("we find " + foundedBook);
            }
        }
        System.out.println("New list book by author " + author);
        return newList;
    }

    public boolean checkAuthor(String authorName) {
        for (int i = 0; i < booksList.size(); i++) {
            Book book = booksList.get(i);
            if (authorName.equals(book.getAuthorName())) {
                return true;
            }
        }
        return false;
    }

    public void giveBookToVisitor(Book book, Visitor visitor) {
        Book gotBook = getBookFromLibrary(book);
        if (gotBook != null) {
            System.out.println("Visitor " + visitor + " add " + book + " to his collection.");
            visitor.addBook(gotBook);
            visitorsMap.put(book, visitor);
            if (visitor.getId() == null) {
                System.out.println("Visitor " + visitor + " got new ID " + startId);
                visitor.setId(startId);
                startId++;
            }
        }
    }

    public void returnBookFromVisitor(Book book, Visitor visitor, int rating) {
        if (booksList.contains(book)) {
            if (visitorsMap.containsKey(book)) {
                visitorsMap.remove(book);
                System.out.println(book + " removed from visitors list.");
                visitor.removeBook(book);
                estimationPart.estimateBook(book,rating);
            } else {
                System.out.println("Visitor list dont have records about " + visitor + " and his book" + book);
            }
        } else {
            System.out.println("Library list dont have " + book);
        }
    }
}
