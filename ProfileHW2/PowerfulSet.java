/**
 * Реализовать класс PowerfulSet, в котором должны быть следующие методы:
 * a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
 * пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
 * Вернуть {1, 2}
 * b. public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
 * объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
 * Вернуть {0, 1, 2, 3, 4}
 * c. public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
 * возвращает элементы первого набора без тех, которые находятся также
 * и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
 */

package ProfileHW2;

import java.util.HashSet;
import java.util.Set;

// Возможно от меня хотели чтоб я вывел реализацию методам retainAll, addAll, removeAll, не совсем понял задание
class PowerfulSet {
    public static void main(String[] args) {
        PowerfulSet powerfulSet = new PowerfulSet();

        Set<String> strings = new HashSet<>();
        strings.add("asd4");
        strings.add("asd1");
        strings.add("asd2");
        strings.add("asd3");
        System.out.println(strings);

        Set<String> strings1 = new HashSet<>();
        strings1.add("asd5");
        strings1.add("asd1");
        strings1.add("asd6");
        strings1.add("asd3");
        System.out.println(strings1);

        Set<String> result = new HashSet<>();
        result = powerfulSet.intersection(strings, strings1);
        System.out.println(result);

        Set<String> result1 = new HashSet<>();
        result1 = powerfulSet.union(strings, strings1);
        System.out.println(result1);

        Set<String> result2 = new HashSet<>();
        result2 = powerfulSet.relativeComplement(strings, strings1);
        System.out.println(result2);
    }

    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> resultSet = new HashSet<>(set1);
        resultSet.retainAll(set2);
        return resultSet;
    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> resultSet = new HashSet<>(set1);
        resultSet.addAll(set2);
        return resultSet;
    }

    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2){
        Set<T> resultSet = new HashSet<>(set1);
        resultSet.removeAll(set2);
        return resultSet;
    }
}
