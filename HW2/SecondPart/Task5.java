package HW2.SecondPart;

import java.util.Scanner;

/**
 * На вход подается число N — количество строк и столбцов матрицы.
 * Затем передается сама матрица, состоящая из натуральных чисел.
 * Необходимо вывести true, если она является симметричной относительно
 * побочной диагонали, false иначе.
 * Побочной диагональю называется диагональ, проходящая из верхнего правого
 * угла в левый нижний.
 */

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите количество столбцов/строк");
        int n = input.nextInt();

        //Построение двумерного массива
        System.out.println("Введите массив");
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = input.nextInt();
            }
        }

        // Проверка массива на симметричность
        boolean isSymmetric = true;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (array[i][j] != array[n - j - 1][n - i - 1]) {
                    isSymmetric = false;
                    break;
                }
            }
        }
        System.out.println(isSymmetric);
    }
}
