package HW2.FirstPart;

import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * строк из N элементов (разделение через перевод строки). Каждая строка
 * содержит только строчные символы латинского алфавита.
 * Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и
 * только один.
 */

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int n = input.nextInt();
        String[] ai = new String[n];
        System.out.println("Введите массив");
        for (int i = 0; i < n; i++){
            ai[i] = input.next();
        }

        int index = -1;
        for (int i = 0; i < ai.length; i++){
            String bufferedString = ai[i];
            for (int j = 0; j < ai.length; j++){
                if (bufferedString.equals(ai[j])){
                    index = j;
                    break;
                }
            }
            if (index != -1){
                System.out.println(ai[index]);
                break;
            }
        }
    }
}
