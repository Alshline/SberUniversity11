package ProfileHW6JDBC;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ManagerClass {

    String createUsersTable = "Create Table if not exists users " +
            "(id serial  primary key ," +
            "firstName varchar not null ," +
            "lastName varchar not null ," +
            "birthDate varchar not null ," +
            "email varchar not null ," +
            "phoneNumber varchar not null," +
            "bookList text [])";
    String createBooksTable = "Create table if not exists books " +
            "(id serial primary key," +
            "BookName varchar(255))";

    public static void main(String[] args) {
        ManagerClass managerClass = new ManagerClass();
    }

    public static Connection getConnection() throws SQLException {
        Properties properties = new Properties();

        String DB_URL = null;
        String USER = null;
        String PASS = null;

        String propertiesPath = "src/main/resources/config.properties";
        try (FileInputStream fileInputStream = new FileInputStream(propertiesPath)) {
            properties.load(fileInputStream);
            DB_URL = properties.getProperty("DB_URL");
            USER = properties.getProperty("USER");
            PASS = "";
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    private void createTable(String SQLExpression) {
        try {
            Statement statement = getConnection().createStatement();
            statement.executeUpdate(SQLExpression);
            statement.close();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
