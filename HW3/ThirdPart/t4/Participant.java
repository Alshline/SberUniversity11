package HW3.ThirdPart.t4;

import java.util.ArrayList;
import java.util.List;

public class Participant implements Comparable<Participant> {
    private final String name;
    private Dog dog = null;
    private final List<Integer> rates;
    private double averageRate;

    public Participant(Dog dog, String name) {
        rates = new ArrayList<>();
        this.dog = dog;
        this.name = name;
        System.out.println("Участник " + name + " с собакой " + dog);
    }

    public Participant(String name) {
        rates = new ArrayList<>();
        this.name = name;
        System.out.println("Участник " + name);
    }

    public void setDog(Dog dog) {
        if (this.dog == null) {
            this.dog = dog;
        } else {
            System.out.println("Участник уже имеет собаку");
        }
    }

    private void checkAverageRate() {
        double sum = 0;
        for (int i = 0; i < rates.size(); i++) {
            sum += rates.get(i);
        }
        this.averageRate = sum / rates.size();
    }

    public double getAverageRate() {
        return averageRate;
    }

    public void addRate(int firstRate, int secondRate, int thirdRate) {
        rates.add(firstRate);
        rates.add(secondRate);
        rates.add(thirdRate);
        checkAverageRate();
    }

    @Override
    public String toString() {
        return name;
    }


    @Override
    public int compareTo(Participant o) {
        if ((this.averageRate - o.averageRate) < 0) {
            return 1;
        } else if ((this.averageRate - o.averageRate) == 0) {
            return 0;
        } else return -1;
    }
}
