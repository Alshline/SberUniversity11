package ProfileHW5;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public class DBManager implements DBManagerInterface, TestInterface {

    // if not exists --> for tests
    String createCustomersTable = "Create Table if not exists Customers " +
            "(id serial  primary key ," +
            "name varchar not null ," +
            "phoneNumber varchar(12) not null)";
    String createOrdersTable = "Create table if not exists Orders " +
            "(Id serial primary key," +
            "CustomerId integer references Customers (Id)," +
            "Flower varchar(10)," +
            "Count integer check (Count > 0 and count < 1001)," +
            "Cost integer," +
            "Date date)";

    private String DB_URL;
    private String USER;
    private String PASS;

    private Connection connection = null;
    private Properties properties = null;

    public DBManager() {
        GetProperties();
        GetConnection();
        DropTables();
        CreateTable(createCustomersTable);
        CreateTable(createOrdersTable);
        InputTestCustomersData();
        InputTestOrdersData();
        //InsertIntoCustomers();
        //DropTables();
    }

    private void GetProperties() {
        properties = new Properties();
        String propertiesPath = "src/main/resources/config.properties";
        try (FileInputStream fileInputStream = new FileInputStream(propertiesPath)) {
            properties.load(fileInputStream);
            this.DB_URL = properties.getProperty("DB_URL");
            this.USER = properties.getProperty("USER");
            this.PASS = "";
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private void GetConnection() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException classNotFoundException) {
            System.out.println("Driver not found");
            classNotFoundException.printStackTrace();
            return;
        }
        System.out.println("Driver good");
        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException sqlException) {
            System.out.println("Failed connection");
            sqlException.printStackTrace();
            return;
        }
        if (connection != null) {
            System.out.println("Connected");
        } else {
            System.out.println("Failed");
        }
    }

    private void CreateTable(String SQLExpression) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(SQLExpression);
            statement.close();
            System.out.println("Table created");
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    private void DropTables() {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("Drop Table if exists Customers, Orders");
            statement.close();
            System.out.println("Tables dropped");
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void InputTestCustomersData() {
        String[] namesArray = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        Statement statement = null;
        try {
            statement = connection.createStatement();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        for (int i = 1; i <= 10; i++) {
            String formString = "insert into customers (name,phonenumber) values ('%s', '%s')";
            try {
                statement.executeUpdate(String.format(formString, namesArray[i - 1], i + 250));
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
        try {
            statement.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void InputTestOrdersData() {
        int[] costsArray = {100, 50, 25};
        String[] flowerNamesArray = {"Roses", "Lilies", "Chamomile"};
        Statement statement = null;
        Random random = new Random();
        try {
            statement = connection.createStatement();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        for (int i = 1; i <= 10; i++) {
            int j = 0;
            int k = 0;
            String formString = "insert into orders (customerid,flower,count,cost,date) values (%d,'%s',%d,%d,current_timestamp)";
            try {
                statement.executeUpdate(String.format(formString,
                        random.nextInt(3) + 1,                       //customerId
                        flowerNamesArray[j = random.nextInt(3)],     //flower
                        k = random.nextInt(50),                      //count of flowers
                        costsArray[j] * k                                 //costs of flowers
                ));
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
        try {
            statement.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void InputOrder() {

    }

    @Override
    public void InputCustomer() {
        Statement statement = null;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input name");
        String name = scanner.nextLine();
        System.out.println("input phone");
        String phone = scanner.nextLine();
        String formString = "insert into customers (name,phonenumber) values('%s','%s')";
        try {
            statement = connection.createStatement();
            statement.executeUpdate(String.format(formString, name, phone));
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void getById(int id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select * from orders inner join customers on (orders.customerid = customers.id) where orders.id = " + id + " ");
            ResultSet resultSet = preparedStatement.executeQuery();
            List resultList = getListFromResultSet(resultSet);
            System.out.println(resultList);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void getByClientPerMonth(int clientId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select * from customers inner join orders on (orders.customerid = customers.id) where customers.id = " + clientId + "and date > current_date - interval '1 month'");
            ResultSet resultSet = preparedStatement.executeQuery();
            List resultList = getListFromResultSet(resultSet);
            System.out.println(resultList);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void getMaxByFlowers() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select * from orders where cost = (select max(cost) from orders)");
            ResultSet resultSet = preparedStatement.executeQuery();
            List resultList = getListFromResultSet(resultSet);
            System.out.println(resultList);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void getRevenue() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select sum(cost) from orders");
            ResultSet resultSet = preparedStatement.executeQuery();
            List resultList = getListFromResultSet(resultSet);
            System.out.println(resultList);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private List getListFromResultSet(ResultSet resultSet) {
        List resultList = new ArrayList();
        ResultSetMetaData resultSetMetaData = null;
        try {
            resultSetMetaData = resultSet.getMetaData();
            int columnCount = resultSetMetaData.getColumnCount();
            while (resultSet.next()) {
                Map<String,Object> rowMap = new HashMap<>();
                for (int i = 1; i <= columnCount; i++){
                    rowMap.put(resultSetMetaData.getColumnLabel(i),resultSet.getObject(i));
                }
                resultList.add(rowMap);
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return resultList;
    }
}
