package ProfileHW3;

import java.lang.annotation.*;

@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface IsLike {
    public boolean IsLikeParent() default false;
}
