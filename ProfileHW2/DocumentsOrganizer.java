/**
 * В некоторой организации хранятся документы (см. класс Document). Сейчас все
 * документы лежат в ArrayList, из-за чего поиск по id документа выполняется
 * неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
 * перевести хранение документов из ArrayList в HashMap.
 * public class Document {
 * public int id;
 * public String name;
 * public int pageCount;
 * }
 * Реализовать метод со следующей сигнатурой:
 * public Map<Integer, Document> organizeDocuments(List<Document> documents)
 */

package ProfileHW2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocumentsOrganizer {
    public static void main(String[] args) {
        Document document1 = new Document(2, "some2", 21);
        Document document = new Document(1, "some1", 1);
        Document document3 = new Document(4, "some4", 44);
        Document document2 = new Document(3, "some3", 3);


        List<Document> documentList = new ArrayList<>();
        documentList.add(document);
        documentList.add(document1);
        documentList.add(document2);
        documentList.add(document3);

        DocumentsOrganizer documentsOrganizer = new DocumentsOrganizer();

        Map<Integer, Document> documentMap = new HashMap<>();
        documentMap = documentsOrganizer.organizeDocuments(documentList);
        documentMap.forEach((key,value) -> System.out.println(value));
    }

    public Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> resultMap = new HashMap<>();
        for (Document document : documents) {
            resultMap.put(document.id, document);
        }
        return resultMap;
    }
}
