package ProfileHW1;

import java.io.*;
import java.util.Locale;
import java.util.Scanner;

public class TryWithResources {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        System.out.println("Input ur input File path");
        File inputFile = new File(input.nextLine());

        String string = new String();

        try {
            input = new Scanner(inputFile);
            while (input.hasNextLine()) {
                string = string.concat(input.nextLine());
            }
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }

        String stringWithUpperCase = string.toUpperCase(Locale.ENGLISH);

        System.out.println("Input ur output File path");
        input = new Scanner(System.in);
        try (Writer fileWriter = new FileWriter(input.nextLine())){
            fileWriter.write(stringWithUpperCase);
            fileWriter.flush();
            input.close();
        }
    }
}
