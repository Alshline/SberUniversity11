/**
 * На вход передается N — количество столбцов в двумерном массиве и M —
 * количество строк. Необходимо вывести матрицу на экран, каждый элемент
 * которого состоит из суммы индекса столбца и строки этого же элемента. Решить
 * необходимо используя ArrayList.
 */

package HW3.ThirdPart.t3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество стобцов массива");
        int N = scanner.nextInt();
        System.out.println("Введите количество строк массива");
        int M = scanner.nextInt();

        List arrayList = new ArrayList();

        for (int i = 0; i < N; i++) {
            List columnList = new ArrayList();
            for (int j = 0; j < M; j++) {
                columnList.add(i + j);
            }
            arrayList.add(columnList);
        }
        for (Object index : arrayList) {
            System.out.println(index);
        }
    }
}
