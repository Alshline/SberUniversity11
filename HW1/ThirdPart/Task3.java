package HW1.ThirdPart;

import java.util.Scanner;

/**
 * На вход подается два положительных числа m и n (m < 10 и n < 10). Необходимо
 * вычислить m^1 + m^2 + ... + m^n.
 */

public class Task3 {

    public static void main(String[] args) {

        System.out.println("Введите два положительных числа");
        Scanner input = new Scanner(System.in);
        int firstNumber = input.nextInt();
        int secondNumber = input.nextInt();

        int sum = 0;

        for (int i = 1; i <= secondNumber; i++){
            sum += (int) Math.pow(firstNumber , i);
        }

        System.out.println(sum);
    }
}
