package HW2.SecondPart;

import java.util.Scanner;

/**
 * На вход подается число N. Необходимо вывести цифры числа справа налево.
 * Решить задачу нужно через рекурсию.
 */
public class Task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите число");
        int n = input.nextInt();
        backOutput(n);
    }

    public static void backOutput(int x) {
        if (x == 0) {
            return;
        } else {
            System.out.print(x % 10 + " ");
            backOutput(x / 10);
        }
    }
}
