package HW2.SecondPart;

import java.util.Scanner;

/**
 * На вход подается число N — количество строк и столбцов матрицы. Затем
 * передается сама матрица, состоящая из натуральных чисел. После этого
 * передается натуральное число P.
 * Необходимо найти элемент P в матрице и удалить столбец и строку его
 * содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
 * Гарантируется, что искомый элемент единственный в массиве.
 */

public class Task4 {
    public static void main(String[] args) {
        //Прием вводных данных
        Scanner input = new Scanner(System.in);
        System.out.println("Введите количество столбцов/строк");
        int n = input.nextInt();

        //Построение двумерного массива
        System.out.println("Введите двумерный массив");
        int[][] table = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                table[i][j] = input.nextInt();
            }
        }

        // Получение искомого элемента
        System.out.println("Введите искомый элемент");
        int p = input.nextInt();

        // Получение координаты искомого элемента
        int[] coordinates = new int[2];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (table[i][j] == p) {
                    coordinates[0] = i;
                    coordinates[1] = j;
                }
            }
        }

        // Построение результирующего массива без выделенных строки и столбца
        int[][] resultArray = new int[n - 1][n - 1];
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if ((j >= coordinates[1]) && (i >= coordinates[0])) {
                    resultArray[i][j] = table[i + 1][j + 1];
                } else if (i >= coordinates[0]) {
                    resultArray[i][j] = table[i + 1][j];
                } else if (j >= coordinates[1]) {
                    resultArray[i][j] = table[i][j + 1];
                } else {
                    resultArray[i][j] = table[i][j];
                }
            }
        }

        // Вывод в консоль
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                System.out.print(resultArray[i][j] + " ");
            }
            System.out.print("\n");
        }
    }
}
