
/**
 * Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
 * (необходимые поля продумать самостоятельно). Обязательно должны быть
 * реализованы валидации на входные параметры.
 * Конструкторы:
 * ● Возможность создать TimeUnit, задав часы, минуты и секунды.
 * ● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
 * должны проставиться нулевыми.
 * ● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
 * должны проставиться нулевыми.
 * Публичные методы:
 * ● Вывести на экран установленное в классе время в формате hh:mm:ss
 * ● Вывести на экран установленное в классе время в 12-часовом формате
 * (используя hh:mm:ss am/pm)
 * ● Метод, который прибавляет переданное время к установленному в
 * TimeUnit (на вход передаются только часы, минуты и секунды).
 */

package HW3.FirstPart;

public class TimeUnit {
    private int hours;
    private int minutes = 0;
    private int seconds = 0;

    public TimeUnit(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        checkTime();
    }

    public TimeUnit(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
        checkTime();
    }

    public TimeUnit(int hours) {
        this.hours = hours;
        checkTime();
    }

    public void getTime() {
        System.out.println(this.hours + ":" + this.minutes + ":" + this.seconds);
    }

    public void getTimeAt12HoursFormat() {
        String timePostFix;
        if (this.hours < 12) {
            timePostFix = "am";
        } else {
            timePostFix = "pm";
        }
        System.out.println(this.hours + ":" + this.minutes + ":" + this.seconds + " " + timePostFix);
    }

    public void addTime(int hours, int minutes, int seconds) {
        this.seconds += seconds;
        this.minutes += minutes;
        this.hours += hours;
        checkTime();
    }

    //Вероятно этот метод добавлен зря, тк этого не было указано в задании, но он приватный, извне его вызвать нельзя
    //Потому, для нормального отображения времени, а не в формате 1000:150:200 оставлю.
    private void checkTime() {
        if (this.seconds / 60 >= 1) {
            this.minutes += this.seconds / 60;
            this.seconds = this.seconds % 60;
        }
        if (this.minutes / 60 >= 1) {
            this.hours += this.minutes / 60;
            this.minutes = this.minutes % 60;
        }
        if (this.hours / 24 >= 1) {
            this.hours = this.hours % 24;
        }
    }
}
