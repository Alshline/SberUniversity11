package HW2.FirstPart;

import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов. После этого передается число M — величина
 * сдвига.
 * Необходимо циклически сдвинуть элементы массива на M элементов вправо.
 */

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int n = input.nextInt();
        int[] ai = new int[n];
        System.out.println("Введите массив");
        for (int i = 0; i < n; i++){
            ai[i] = input.nextInt();
        }
        System.out.println("Введите сдвиг");
        int m = input.nextInt();

        int[] bufferedArray = new int[n];
        for (int i = 0; i < ai.length; i++){
            int index = i + m;
            if (index >= ai.length){
                index = index - ai.length;
            }
            bufferedArray[index] = ai[i];
        }

        for (int j = 0; j < bufferedArray.length; j++){
            System.out.print(bufferedArray[j]);
        }
    }
}
