package HW3.ThirdPart.t4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Judges {
    private final List<Participant> participantList;

    public Judges() {
        participantList = new ArrayList<>();
    }

    public Judges addParticipant(Participant participant) {
        participantList.add(participant);
        return this;
    }

    public List getWinnersList() {
        Collections.sort(participantList);
        List winnersList = new ArrayList();
        for (int i = 0; i < 3; i++) {
            winnersList.add(this.participantList.get(i));
        }
        return winnersList;
    }
}
