/**
 * На вход подается список целых чисел. Необходимо вывести результат перемножения
 * этих чисел.
 * Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
 * 120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */

package ProfileHW4;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamListSum {
    public static int getSumFromList(List<Integer> listWithIntegers) {
        List<Integer> copyList = Optional.ofNullable(listWithIntegers)
                .map(List::stream)
                .orElseGet(Stream::empty)
                .collect(Collectors.toList());
        Integer result = copyList.stream()
                .mapToInt(Integer::intValue)
                .filter(x -> x % 2 == 0)
                .sum();
        return result;
    }

}
