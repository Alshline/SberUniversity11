package ProfileHW6;

import java.util.List;
import java.util.Optional;

public interface DaoInterface<T> {
    T add (T t);
    void delete(long id);
    T get(long id);
    void update(T t);
    List<T> getAll();
}
