package ProfileHW2;

import java.util.Arrays;

public class IsAnagram {

    //Включая пробелы (полная анаграмма) иначе применить replaceAall("\\s"," ") и после toLowerCase()
    public static boolean isAnagramm(String str1, String str2) {
        char[] chars = str1.toCharArray();
        char[] chars1 = str2.toCharArray();
        if (chars.length != chars1.length) return false;
        Arrays.sort(chars);
        Arrays.sort(chars1);
        if (Arrays.equals(chars,chars1)) return true;
        else return false;
    }

    public static void main(String[] args) {
        String str1 = new String("123454321");
        String str2 = "123454 321";

        System.out.println(isAnagramm(str1,str2));
    }
}
