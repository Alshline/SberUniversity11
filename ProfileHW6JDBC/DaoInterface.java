package ProfileHW6JDBC;

import java.sql.SQLException;
import java.util.List;

public interface DaoInterface<T> {

    T get(Integer id) throws SQLException;
    List<T> getAll();
    void add(T t);
    void update(T t);
    void delete(Integer id);
}
