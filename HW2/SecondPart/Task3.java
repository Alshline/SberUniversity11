package HW2.SecondPart;

import java.util.Scanner;

/**
 * На вход подается число N — количество строк и столбцов матрицы. Затем
 * передаются координаты X и Y расположения коня на шахматной доске.
 * Необходимо заполнить матрицу размера NxN нулями, местоположение коня
 * отметить символом K, а позиции, которые он может бить, символом X.
 */

public class Task3 {
    public static void main(String[] args) {
        //Прием вводных данных
        Scanner input = new Scanner(System.in);
        System.out.println("Введите количество столбцов/строк");
        int n = input.nextInt();

        //Построение двумерного массива с полем
        char[][] table = new char[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                table[i][j] = '0';
            }
        }

        // Получение координат коня
        int[] startCoordinate = new int[2];
        System.out.println("Введите координату коня");
        for (int i = 0; i < 2; i++) {
            startCoordinate[i] = input.nextInt();
        }

        // Поле с конем
        int[][] horseTable = {{+3, -1},
                {+3, +1},
                {-3, -1},
                {-3, +1},
                {-1, +3},
                {+1, +3},
                {-1, -3},
                {+1, -3}};

        // Вставка в поле
        for (int i = 0; i <= 7; i++) {
            int x = startCoordinate[1] + horseTable[i][1];
            int y = startCoordinate[0] + horseTable[i][0];
            if ((x > n) || (x < 0) || (y > n) || (y < 0)) {
                continue;
            } else {
                table[x][y] = 'X';
            }
        }
        table[startCoordinate[1]][startCoordinate[0]] = 'K';

        // Вывод поля
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.print("\n");
        }
    }
}
