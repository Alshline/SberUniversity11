package HW2.FirstPart;

import java.util.Scanner;

/**
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
 * вводится число X — элемент, который нужно добавить в массив, чтобы
 * сортировка в массиве сохранилась.
 * Необходимо вывести на экран индекс элемента массива, куда нужно добавить
 * X. Если в массиве уже есть число равное X, то X нужно поставить после уже
 * существующего.
 */

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите длину первого массива");
        int n = input.nextInt();
        int[] ai = new int[n];
        System.out.println("Введите массив");
        for (int i = 0; i < n; i++){
            ai[i] = input.nextInt();
        }
        System.out.println("Введите вставляемый элемент");
        int element = input.nextInt();

        //Detect index of closest element
        int index = 0;
        for (int i = 0; i < ai.length; i++){
            if (element > ai[i]){
                index = i;
            }
        }

        System.out.println(index+1);
    }
}
