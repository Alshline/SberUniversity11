package ProfileHW6JDBC;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements DaoInterface<User> {
    @Override
    public User get(Integer id) {
        try {
            Connection connection = ManagerClass.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users WHERE users.id=" + id);
            if (resultSet.next()) {
                return getUserFromResultSet(resultSet);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public User getByEmail(String email) {
        try {
            Connection connection = ManagerClass.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users WHERE users.email='" + email + "'");
            if (resultSet.next()) {
                return getUserFromResultSet(resultSet);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public List<User> getAll() {
        try {
            List<User> usersList = new ArrayList<>();
            Connection connection = ManagerClass.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");
            while (resultSet.next()) {
                usersList.add(getUserFromResultSet(resultSet));
            }
            return usersList;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public void add(User user) {
        Statement statement = null;
        String formString = "insert into users (firstname, lastname, birthdate, email,phonenumber) values('%s','%s','%s','%s','%s')";
        try {
            statement = ManagerClass.getConnection().createStatement();
            statement.executeUpdate(String.format(formString,
                    user.getFirstName(),
                    user.getLastName(),
                    user.getDateOfBirth(),
                    user.getEmail(),
                    user.getPhoneNumber()));
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void update(User user) {
        Statement statement = null;
        String formString = "UPDATE users SET firstname='%s', lastname='%s', birthdate='%s', email='%s', phonenumber='%s' WHERE id='%d'";
        try {
            statement = ManagerClass.getConnection().createStatement();
            statement.executeUpdate(String.format(formString,
                    user.getFirstName(),
                    user.getLastName(),
                    user.getDateOfBirth(),
                    user.getEmail(),
                    user.getPhoneNumber()));
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Connection connection = ManagerClass.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM users WHERE id=" + id);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    private User getUserFromResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setFirstName(resultSet.getString("firstname"));
        user.setLastName(resultSet.getString("lastname"));
        user.setDateOfBirth(resultSet.getString("birthdate"));
        user.setEmail(resultSet.getString("email"));
        user.setPhoneNumber(resultSet.getString("phonenumber"));
        return user;
    }
}
