
/**
 * Необходимо реализовать класс Cat.
 * У класса должны быть реализованы следующие приватные методы:
 * ● sleep() — выводит на экран “Sleep”
 * ● meow() — выводит на экран “Meow”
 * ● eat() — выводит на экран “Eat”
 * И публичный метод:
 * status() — вызывает один из приватных методов случайным образом.
 */

package HW3.FirstPart;

public class Cat {
    private static void sleep() {
        System.out.println("Sleep");
    }

    private static void meow() {
        System.out.println("Meow");
    }

    private static void eat() {
        System.out.println("Eat");
    }

    public static void status() {
        int randomNumber = (int) (Math.random() * 3);
        switch (randomNumber) {
            case 0:
                eat();
                break;
            case 1:
                meow();
                break;
            case 2:
                sleep();
                break;
        }
    }
}
