package HW2.SecondPart;

import java.util.Scanner;

/**
 * На вход подается число N. Необходимо посчитать и вывести на экран сумму его
 * цифр. Решить задачу нужно через рекурсию.
 */

public class Task8 {
    public static void main(String[] args) {
        //Получение числа n, способов иных не смог найти
        Scanner input = new Scanner(System.in);
        System.out.println("Введите число");
        int n = input.nextInt();
        System.out.println(factorial(n));
    }

    // Отдельный метод
    public static int factorial(int x) {
        int result = 0;
        if (x == 1) {
            return 1;
        } else {
            result = factorial(x - 1) * x;
        }
        return result;
    }
}
