/**
 * Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
 */

package ProfileHW4;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class GetSetFromSetOfSet {
    public static void main(String[] args) {
        Set<Set<Integer>> mainSet = new HashSet<>();
        Set<Integer> set1 = Set.of(1,2,3,4,5);
        Set<Integer> set2 = Set.of(6,7,8,9,15);
        Set<Integer> set3 = Set.of(10,11,12,13,14);
        Set<Integer> set4 = Set.of(1,2,3,4,5);
        mainSet.add(set1);
        mainSet.add(set2);
        mainSet.add(set3);
        mainSet.add(set4);
        System.out.println(getSetsFromSet(mainSet));
    }
    public static Set<Integer> getSetsFromSet(Set<Set<Integer>> setWithSets){
        Set<Integer> resultSet = new HashSet<>();
        Stream.of(setWithSets).flatMap(Set::stream).forEach(resultSet::addAll);
        return resultSet;
    }
}
