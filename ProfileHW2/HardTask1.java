/**
 * Реализовать метод, который принимает массив words и целое положительное число k.
 * Необходимо вернуть k наиболее часто встречающихся слов..
 * Результирующий массив должен быть отсортирован по убыванию частоты
 * встречаемого слова. В случае одинакового количества частоты для слов, то
 * отсортировать и выводить их по убыванию в лексикографическом порядке.
 */

package ProfileHW2;

import java.util.*;

public class HardTask1 {
    public String[] returnArrangedArray (String[] words,int countOfWords){
        Map<String, Integer> resultMap = new HashMap<>();
        for (String string : words){
            int count = 1;
            if (resultMap.containsKey(string)){
                count = resultMap.get(string);
                count++;
            }
            resultMap.put(string,count);
        }
        List<String> listOfWords = new ArrayList<>();
        for (Map.Entry<String,Integer> entry : resultMap.entrySet()){
            if (entry.getValue()<=countOfWords) {
                listOfWords.add(entry.getKey());
            }
        }
        Collections.sort(listOfWords);
        List<String> resultList = new ArrayList<>();
        for (int i = 0; i < countOfWords; i++){
            resultList.add(listOfWords.get(i));
        }
        String[] stringArray = new String[resultList.size()];
        resultList.toArray(stringArray);
        return  stringArray;
    }

    public static void main(String[] args) {
        String[] strings = {"the","day","is","sunny","the","the","the",
                "sunny","is","is","day"};
        HardTask1 hardTask1 = new HardTask1();
        String[] result = hardTask1.returnArrangedArray(strings,4);
        for (String s : result){
            System.out.println(s);
        }
    }
}
