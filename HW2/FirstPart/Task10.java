package HW2.FirstPart;

import java.util.Scanner;

/**
 * Необходимо реализовать игру. Алгоритм игры должен быть записан в
 * отдельном методе. В методе main должен быть только вызов метода с
 * алгоритмом игры.
 * Условия следующие:
 * Компьютер «загадывает» (с помощью генератора случайных чисел) целое
 * число M в промежутке от 0 до 1000 включительно. Затем предлагает
 * пользователю угадать это число. Пользователь вводит число с клавиатуры.
 * Если пользователь угадал число M, то вывести на экран "Победа!". Если
 * введенное пользователем число меньше M, то вывести на экран "Это число
 * меньше загаданного." Если введенное число больше, то вывести "Это число
 * больше загаданного." Продолжать игру до тех пор, пока число не будет отгадано
 * или пока не будет введено любое отрицательное число.
 */

public class Task10 {
    public static void main(String[] args) {
        game();
    }

    public static void game(){
        double number = (int) (Math.random()*1001);
        Scanner input = new Scanner(System.in);

        int inputNumber;
        boolean flag = true;
        while (flag){
            System.out.println("Введите число");
            inputNumber = input.nextInt();
            if (inputNumber < 0){
                break;
            }

            if (inputNumber > number){
                System.out.println("Это число больше загаданного.");
            } else if (inputNumber < number){
                System.out.println("Это число меньше загаданного.");
            } else if (inputNumber == number){
                System.out.println("Победа!");
                flag = false;
            }
        }
    }
}
