package HW3.ThirdPart.t2;

public class BestCarpenter {

    public BestCarpenter() {
        System.out.println("Цех создан");
    }

    public boolean isCarpenterCanRepair(Furniture furniture) {
        if (furniture instanceof Table) {
            System.out.println("We cant repair that");
            return false;
        } else {
            return true;
        }
    }
}
