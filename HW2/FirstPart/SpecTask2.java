package HW2.FirstPart;

import java.util.Scanner;

/**
 * Решить задачу 7 основного дз за линейное время.
 */

// Получаем несколько лишних процедур, но скорость, тем не менее, линейная
public class SpecTask2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int n = input.nextInt();
        int[] ai = new int[n];
        System.out.println("Введите массив");
        for (int i = 0; i < n; i++){
            ai[i] = input.nextInt();
        }

        // Get new array
        int[] bufferedArray = new int[n];
        for (int i = 0; i < ai.length; i++){
            bufferedArray[i] = ai[i]*ai[i];
        }

        for (int i = 0; i < bufferedArray.length; i++){
            int index_i = i;

            for (int j = i+1; j < ai.length; j++){
                if (bufferedArray[index_i] > bufferedArray[j]){
                    index_i = j;
                }
            }
            if (i != index_i){
                int tempNumber = bufferedArray[i];
                bufferedArray[i] = bufferedArray[index_i];
                bufferedArray[index_i] = tempNumber;
            }
        }

        for (int j = 0; j < bufferedArray.length; j++){
            System.out.print(bufferedArray[j] + " ");
        }
    }
}
