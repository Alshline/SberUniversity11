package HW3.ThirdPart.t1.someAnimals;

import HW3.ThirdPart.t1.BirdWayOfBirth;
import HW3.ThirdPart.t1.FlyingAnimals;

public class Eagle extends FlyingAnimals implements BirdWayOfBirth {
    public Eagle() {

    }

    @Override
    public void flying() {
        System.out.println("fastest flying");
    }
}
