package HW1.ThirdPart;

import java.util.Scanner;

/**
 * Дано натуральное число. Вывести его цифры в “столбик”.
 */

public class Task4 {

    public static void main(String[] args) {

        System.out.println("Введите ваше число");
        Scanner input = new Scanner(System.in);
        String number = input.nextLine();
        int count = 0;

        while (count < number.length()){
            System.out.println(number.charAt(count));
            count++;
        }
    }
}
