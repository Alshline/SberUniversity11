package HW2.SecondPart;

/**
 * На вход передается N — количество столбцов в двумерном массиве и M —
 * количество строк. Затем сам передается двумерный массив, состоящий из
 * натуральных чисел.
 * Необходимо сохранить в одномерном массиве и вывести на экран
 * минимальный элемент каждой строки.
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        //Прием вводных данных
        Scanner input = new Scanner(System.in);
        System.out.println("Введите количество столбцов");
        int m = input.nextInt();
        System.out.println("Введите количество строк");
        int n = input.nextInt();
        int[][] ai = new int[n][m];
        System.out.println("Введите массив");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                ai[i][j] = input.nextInt();
            }
        }
        // Получение минимальных значений
        int[] minimalNumbers = new int[n];
        for (int i = 0; i < n; i++) {
            minimalNumbers[i] = ai[i][0];
            for (int j = 1; j < m; j++) {
                if (ai[i][j] < minimalNumbers[i]) {
                    minimalNumbers[i] = ai[i][j];
                }
            }
        }
        // Получить результат через for each у меня не вышло, было бы здорово если бы вы сказали - возможно ли получить нормальный результат используя итераторы.

        //Вывод полученного массива с минимальным значением (итератор)
        for (int a : minimalNumbers) {
            System.out.print(minimalNumbers[a] + " ");
        }
    }
}
