package ProfileHW6;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class BookDao implements DaoInterface<Book>{
    private EntityManager entityManager =
            Persistence.createEntityManagerFactory("SberUniversity")
                    .createEntityManager();
    private List<Book> books = new ArrayList<>();

    @Override
    public Book add(Book book) {
        entityManager.getTransaction().begin();
        Book bookFromDB = entityManager.merge(book);
        entityManager.getTransaction().commit();
        return bookFromDB;
    }

    @Override
    public void delete(long id) {
        entityManager.getTransaction().begin();
        entityManager.remove(get(id));
        entityManager.getTransaction().commit();
    }

    @Override
    public Book get(long id) {
        return entityManager.find(Book.class,id);
    }

    @Override
    public void update(Book book) {
        entityManager.getTransaction().begin();
        entityManager.merge(book);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<Book> getAll() {
        TypedQuery<Book> bookTypedQuery = entityManager.createNamedQuery("Book.getAll",Book.class);
        return bookTypedQuery.getResultList();
    }
}
