package HW1.ThirdPart;

import java.util.Scanner;

/**
 *В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
 * число n - количество денег для размена. Необходимо найти минимальное
 * количество купюр с помощью которых можно разменять это количество денег
 * (соблюсти порядок: первым числом вывести количество купюр номиналом 8,
 * вторым - 4 и т д)
 */

public class Task6 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Введите количество денег");
        int cash = input.nextInt();

        //first method (возможно, можно сделать рекурсивным, но не успеваю)

        int count_cash_8 = (cash - cash % 8) / 8;
        int firstBalance = cash - count_cash_8 * 8;

        int count_cash_4 = (firstBalance - firstBalance % 4) / 4;
        int secondBalance = firstBalance - count_cash_4*4;

        int count_cash_2 = (secondBalance - secondBalance %2) / 2;
        int thirdBalance = secondBalance - count_cash_2*2;

        int count_cash_1 = thirdBalance;

        System.out.println("Через формулы: " + count_cash_8 + " " + count_cash_4 + " " + count_cash_2 + " " + count_cash_1);

        // Second method

        int amount_8 = 0;
        int amount_4 = 0;
        int amount_2 = 0;
        int amount_1 = 0;

        for (; cash >= 1; amount_1++){

            for (; cash >= 2; amount_2++){

                for (; cash >= 4; amount_4++){

                    for (; cash >= 8; amount_8++){
                        cash -= 8;
                    }

                    // костыль
                    if (cash >= 4){
                        cash -= 4;
                    } else break;

                }

                cash -= 2;
            }

            cash -= 1;
        }

        System.out.println("Второй метод: " + amount_8 + " " + amount_4 + " " + amount_2 + " " + amount_1);
    }
}
