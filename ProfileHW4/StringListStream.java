/**
 * На вход подается список непустых строк. Необходимо привести все символы строк к
 * верхнему регистру и вывести их, разделяя запятой.
 * Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */

package ProfileHW4;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class StringListStream {
    public static void main(String[] args) {
        List<String> stringList = List.of("abc", "def", "qqq");
        System.out.println(getUpperCaseStringFromList(stringList));
    }

    public static String getUpperCaseStringFromList(List<String> stringList) {
        return stringList.stream().collect(Collectors.joining(",")).toUpperCase(Locale.ROOT);
    }
}
