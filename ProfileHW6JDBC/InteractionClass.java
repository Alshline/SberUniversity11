package ProfileHW6JDBC;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InteractionClass {
    public static void main(String[] args) throws SQLException {
        InteractionClass interactionClass = new InteractionClass();
        List<Book> list = interactionClass.getBookFromUserByEmail("email5");
        System.out.println(list);
    }

    //horrible name
    public void insertBooksIntoUser(User user, Book book) {
        Statement statement = null;
        String formString = "UPDATE users SET booklist = array_append(booklist, '%s') WHERE users.id =" + user.getId();
        try {
            statement = ManagerClass.getConnection().createStatement();
            statement.executeUpdate(String.format(formString, book.getBookName()));
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public List<Book> getBookFromUserByEmail(String email) throws SQLException {
        UserDao userDao = new UserDao();
        User user = userDao.getByEmail(email);
        try {
            Connection connection = ManagerClass.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select booklist from users where users.id =" + user.getId());
            List<String> stringList = new ArrayList<>();
            if (resultSet.next()) {
                Array resultArray = resultSet.getArray("booklist");
                String[] resultStringArray = (String[]) resultArray.getArray();
                stringList = Arrays.stream(resultStringArray).toList();
            }
            BookDao bookDao = new BookDao();
            List<Book> bookList = new ArrayList<>();
            for (String name : stringList) {
                bookList.add(bookDao.getByName(name));
            }
            return bookList;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
